xbindkeys for Debian
--------------------

You may want to install the xbindkeys_config Package to have a GUI for
the Setup of xbindkeys.

xbindkeys autostart
-------------------
Within systems following the Freedesktop standard of application
autostart, xbindkeys should load automatically. In other environments
the user has to take care of this themself, either by just adding
xbindkeys to their .xsession - or by calling
/usr/bin/xbindkeys_autostart via whatever way their environment provides.

Using xbindkeys_autostart instead of calling xbindkeys directly adds
the possibility to easily disable xbindkeys by touching
~/.xbindkeys.noauto, other than that there is no difference.

Modifier keys
-------------
If you have a modifier Key which is not known to xbindkeys by its name
you can go "The Hard Way":

======
First, be sure that it generates events in 'xev'. Run 'xev' from a terminal
emulator, and hit the modifier key. If it generates text, then you're set to
use it with xbindkeys.

The output 'xev' gives is important. When you push down the modifier key, it
generates a "KeyPress" event.  When you release it, it generates a
"KeyRelease" event. Note that the "KeyRelease" event is the important one. The
"state" field in this paragraph is what we need to give xbindkeys. Here's some
example output of my setup:

KeyPress event, serial 28, synthetic NO, window 0x1a00001,
    root 0x40, subw 0x0, time 3415640395, (89,156), root:(929,614),
    state 0x10, keycode 115 (keysym 0xffed, Hyper_L), same_screen YES,
    XLookupString gives 0 characters:  ""

KeyRelease event, serial 28, synthetic NO, window 0x1a00001,
    root 0x40, subw 0x0, time 3415640637, (89,156), root:(929,614),
    state 0x30, keycode 115 (keysym 0xffed, Hyper_L), same_screen YES,
    XLookupString gives 0 characters:  ""

So, we care about the KeyRelease event. The state is set to "0x30". So, in
~/.xbindkeysrc, we have:

"xterm &"
  m:0x30 + F1

There we go, now I can use my un-named modifier key with xbindkeys. For what
it's worth, my "un-named modifier key" is the left Windows key.
======

Today these Modifier Keys are known by name:
Control, Shift, Mod1 (Alt), Mod2 (NumLock), Mod3 (CapsLock), Mod4,
Mod5 (Scroll).

 -- Joerg Jaspert <joerg@debian.org>, Sun, 16 Feb 2014 13:24:12 +0100
