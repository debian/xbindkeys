Source: xbindkeys
Section: x11
Priority: optional
Maintainer: Joerg Jaspert <joerg@debian.org>
Uploaders: Ricardo Mones <mones@debian.org>
Build-Depends: debhelper-compat (= 13), libx11-dev, libxt-dev, guile-3.0-dev
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://www.nongnu.org/xbindkeys/
Vcs-Git: https://salsa.debian.org/debian/xbindkeys.git
Vcs-Browser: https://salsa.debian.org/debian/xbindkeys

Package: xbindkeys
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: wish, xbindkeys-config
Description: Associate a combination of keys or mouse buttons with a shell command
 xbindkeys is a program that allows you to launch shell commands with
 your keyboard or your mouse under the X Window System.
 It links commands to keys or mouse buttons, using a configuration file.
 It's independent of the window manager and can capture all keyboard keys
 (ex: Power, Wake...).
 .
 It optionally supports a guile-based configuration file layout, which enables
 you to access all xbindkeys internals, so you can have key combinations,
 double clicks or timed double clicks take actions. Also all functions that work
 in guile will work for xbindkeys.
